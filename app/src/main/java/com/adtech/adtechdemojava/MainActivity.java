package com.adtech.adtechdemojava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adtech.adtechmobile.*;

import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity {
    private ADTInterstitial adtInterAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ADTechMobile.initialize(this);
        adtInterAd = new ADTInterstitial(this);
        ADTRequestConfiguration.testMode = true;

        testBannerXML();
        testBannerProgrammatic();
        testInterstitial();
    }

    private void testBannerXML() {
        ADTBannerView adView = findViewById(R.id.bannerAd);
        adView.setAdSize(ADTAdSize.HEIGHT_50);
        adView.setAdListener(new ADTAdListener() {
            @Override
            public void onAdFailedToLoad(@NotNull ADTAdError adtAdError) {
                Log.d("Test App", "ADT BannerXML failed: code " + adtAdError);
            }

            @Override
            public void onAdClicked() {
                Log.d("Test App", "ADT BannerXML clicked");
            }
            @Override
            public void onAdLoaded() {
                Log.d("Test App", "ADT BannerXML loaded");
            }

            @Override
            public void onRewardedAdUserEarnedReward() {}

            @Override
            public void onRewardedAdFailedToShow(ADTAdError var1) {}

            @Override
            public void onNativeAdLoaded(ADTNativeAd adtNativeAd) { }

            @Override
            public void onAdOpened() { }

            @Override
            public void onAdLeftApplication() { }

            @Override
            public void onAdClosed() { }
        });
        adView.request();
    }

    private void testBannerProgrammatic() {
        ADTBannerView adView = new ADTBannerView(this);
        // Tell parent layout how to render the adview
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        adView.setLayoutParams(layoutParams);
        ViewGroup layout = (ViewGroup) findViewById(R.id.activityLayout);
        // Add to parent layout
        layout.addView(adView);
        adView.setAdSize(ADTAdSize.HEIGHT_90);
        adView.setAdListener(new ADTAdListener() {
            @Override
            public void onAdLoaded() {
                Log.d("Test App", "ADT Banner loaded");
            }

            @Override
            public void onAdFailedToLoad(@NotNull ADTAdError adtAdError) {
                Log.d("Test App", "ADT Banner failed: code " + adtAdError);
            }

            @Override
            public void onAdClicked() {
                Log.d("Test App", "ADT Banner clicked");
            }

            @Override
            public void onNativeAdLoaded(ADTNativeAd adtNativeAd) { }

            @Override
            public void onRewardedAdUserEarnedReward() { }

            @Override
            public void onRewardedAdFailedToShow(ADTAdError adtAdError) { }


            @Override
            public void onAdOpened() { }

            @Override
            public void onAdLeftApplication() { }

            @Override
            public void onAdClosed() { }
        });
        adView.request();

    }

    private void testInterstitial() {
        adtInterAd.setAdListener(new ADTAdListener() {
            @Override
            public void onAdLoaded() {
                Log.d("Test App", "ADT Interstitial loaded");
            }

            @Override
            public void onAdFailedToLoad(@NotNull ADTAdError adtAdError) {
                Log.d("Test App", "ADT Interstitial failed: code " + adtAdError);
            }

            @Override
            public void onAdClosed() {
                adtInterAd.request();
            }

            @Override
            public void onNativeAdLoaded(ADTNativeAd adtNativeAd) { }

            @Override
            public void onRewardedAdUserEarnedReward() { }

            @Override
            public void onRewardedAdFailedToShow(ADTAdError adtAdError) { }

            @Override
            public void onAdClicked() { }

            @Override
            public void onAdOpened() { }

            @Override
            public void onAdLeftApplication() { }
        });
        adtInterAd.request();

        Button myButton = findViewById(R.id.myButton);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adtInterAd.isReady()) {
                    adtInterAd.show();
                }
            }
        });
    }
}